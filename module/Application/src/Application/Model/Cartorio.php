<?php

namespace Application\Model;

class Cartorio
{

    /**
     * @var $id
     * @type int
     */
    public $id;

    /**
     * @var $nome
     * @type string
     */
    public $nome;

    /**
     * @var $razao
     * @type string
     */
    public $razao;

    /**
     * @var $tipo_documento
     * @type int
     */
    public $tipo_documento;

    /**
     * @var $documento
     * @type string
     */
    public $documento;

    /**
     * @var $cep
     * @type string
     */
    public $cep;

    /**
     * @var $endereco
     * @type string
     */
    public $endereco;

    /**
     * @var $bairro
     * @type string
     */
    public $bairro;

    /**
     * @var $cidade
     * @type string
     */
    public $cidade;

    /**
     * @var $uf
     * @type string
     */
    public $uf;

    /**
     * @var $tabeliao
     * @type string
     */
    public $tabeliao;

    /**
     * @var $ativo
     * @type bool
     */
    public $ativo;

    public function exchangeArray($data = array())
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->nome = (!empty($data['nome'])) ? $data['nome'] : null;
        $this->razao = (!empty($data['razao'])) ? $data['razao'] : null;
        $this->tipo_documento = (!empty($data['tipo_documento'])) ? $data['tipo_documento'] : null;
        $this->documento = (!empty($data['documento'])) ? $data['documento'] : null;
        $this->cep = (!empty($data['cep'])) ? $data['cep'] : null;
        $this->endereco = (!empty($data['endereco'])) ? $data['endereco'] : null;
        $this->bairro = (!empty($data['bairro'])) ? $data['bairro'] : null;
        $this->cidade = (!empty($data['cidade'])) ? $data['cidade'] : null;
        $this->uf = (!empty($data['uf'])) ? $data['uf'] : null;
        $this->tabeliao = (!empty($data['tabeliao'])) ? $data['tabeliao'] : null;
        $this->ativo = (!empty($data['ativo'])) ? $data['ativo'] : null;
    }
}