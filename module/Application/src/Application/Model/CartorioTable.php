<?php
/**
 * Created by PhpStorm.
 * User: lucascampelo
 * Date: 07/04/18
 * Time: 12:38
 */

namespace Application\Model;

use Zend\Db\Sql\Insert;
use Zend\Db\TableGateway\TableGateway;

class CartorioTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function delete($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }

    public function saveAll($arrCartorios)
    {
        $arrayCnpj = array_map(function($cartorio) {
            return $cartorio['documento'];
        }, $arrCartorios);

        $resultSet = $this->tableGateway->select(array(
            'documento' => $arrayCnpj
        ));

        // Realiza o update dos registros já existentes no Banco
        $arrayCnpjUpdate = array();
        if ($resultSet->count()) {
            foreach ($resultSet as $cartorio) {
                $arrayCnpjUpdate[] = $cartorio->documento;
            }
            $totalRowsToUpdate = count($arrayCnpjUpdate);
            $updatedRows = 0;

            foreach ($arrCartorios as $arrCartorio) {
                if (!in_array($arrCartorio['documento'], $arrayCnpjUpdate)) {
                    continue;
                }

                $this->tableGateway->update($arrCartorio, array(
                    'documento' => $arrCartorio['documento'],
                ));

                // Finaliza o laço assim que todos os registros (do array `$arrayCnpjUpdate`) forem atualizados
                if (++$updatedRows == $totalRowsToUpdate) {
                    break;
                }
            }
        }

        // Realiza o DIFF para saber quais são os novos registros
        $toInsert = array_diff($arrayCnpj, $arrayCnpjUpdate);

        // Insere só os novos registros
        foreach ($arrCartorios as $arrCartorio) {
            if (!in_array($arrCartorio['documento'], $toInsert)) {
                continue;
            }

            $this->tableGateway->insert($arrCartorio);
        }

    }

}