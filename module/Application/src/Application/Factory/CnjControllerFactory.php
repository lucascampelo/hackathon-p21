<?php

namespace Application\Factory;

use Application\Controller\CnjController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class CnjControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $realServiceLocator = $serviceLocator->getServiceLocator();
        $xmlService        = $realServiceLocator->get('Application\Service\Xml');

        return new CnjController($xmlService);
    }

}