<?php

namespace Application\Controller;

use Application\Form\ImportarXmlForm;
use Application\Service\Xml as XmlService;
use Zend\Http\PhpEnvironment\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class CnjController extends AbstractActionController
{
    /**
     * @var XmlService
     */
    protected $xmlService;

    public function __construct(XmlService $xmlService)
    {
        $sm = $this->getServiceLocator();
        $this->xmlService = $xmlService;
    }


    public function importarXmlAction()
    {
        $form = new ImportarXmlForm();
        $msgs = array();

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $arquivoXml = $request->getFiles('arquivoXml');
            if ($arquivoXml && is_uploaded_file($arquivoXml['tmp_name'])) {
                if (UPLOAD_ERR_OK !== $arquivoXml['error']) {
                    $msgs[] = array(
                        'type' => 'error',
                        'content' => 'Ocorreu um erro ao enviar o XML.',
                    );

                    return new ViewModel(compact('form','errors'));
                }

                $result = $this->xmlService->importarXml($arquivoXml['tmp_name']);
                unlink($arquivoXml['tmp_name']);

                if ($result) {
                    $msgs[] = array(
                        'type' => 'success',
                        'content' => 'Dados importados com sucesso!'
                    );
                }
            }
        }

        return new ViewModel(compact('form'));
    }


}

