<?php
/**
 * Created by PhpStorm.
 * User: lucascampelo
 * Date: 07/04/18
 * Time: 10:49
 */

namespace Application\Form;

use Zend\Form\Form;

class ImportarXmlForm extends Form
{
    public function __construct($name = 'importarXml', array $options = array())
    {
        parent::__construct($name, $options);

        $this->add(array(
            'name' => 'arquivoXml',
            'type' => 'File',
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Enviar',
            ),
        ));
    }


}