<?php

namespace Application\Service;

use Application\Model\CartorioTable;
use Zend\File\Exception\InvalidArgumentException;

class Xml
{
    /**
     * @var cartorioTable
     * @type CartorioTable
     */
    private $cartorioTable;

    public function __construct(CartorioTable $cartorioTable)
    {
        $this->cartorioTable = $cartorioTable;
    }

    public function importarXml($filePath = null)
    {
        // Valida se o arquivo realmente existe
        if (!is_file($filePath)) {
            throw new InvalidArgumentException('Arquivo não encontrado.');
        }

        $xml = simplexml_load_file($filePath);

        // Valida o XML
        if (
            false === $xml ||
            !isset($xml->cartorio) ||
            count($xml->cartorio) == 0
        ) {
            throw new InvalidArgumentException('Formato do xml inválido.');
        }

        // Converte o XML em Array para inserir no Banco de dados
        $databaseRows = [];

        $campos = array('nome','razao','tipo_documento','documento','cep','endereco','bairro','cidade','uf','tabeliao',
            'ativo'
        );

        // Varre as linhas do XML
        foreach ($xml->cartorio as $cartorio) {
            $row = array();
            // Varre os campos do registro
            foreach ($campos as $campo) {
                if (isset($cartorio->{$campo})) {
                    $row[$campo] = trim((string) $cartorio->{$campo});
                }
            }
            $databaseRows[] = $row;
        }

        return $this->cartorioTable->saveAll($databaseRows);
    }
}