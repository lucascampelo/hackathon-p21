<?php

namespace Application\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class XmlFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $cartorioTable        = $serviceLocator->get('Application\Model\CartorioTable');

        return new Xml($cartorioTable);
    }
}